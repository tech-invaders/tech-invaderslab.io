---
title: Discovery and Implementation of a Dynamic Graph with Angular
date: 2024-03-03 14:50:43
tags:
  - front-end
  - angular
  - ngx-graph
  - graph
lang: en
authors:
  - claire
---


Recently, I challenged myself to implement a dynamic graph in an Angular application. The idea was to create a **Directed Acyclic Graph (DAG)** capable of adapting to various scenarios while providing user interactivity. In this article, I will share my journey from finding the ideal solution to successfully integrating it into my project.

{% img center ngx-graph/graph-UX.png graph %}

## Quest for the Solution

I embarked on this adventure believing that there must be a solution to meet my needs. My goal was clear: integrate a dynamic graph into my Angular application, capable of meeting the following requirements:

- Designing a flexible graph adaptable to different scenarios.
- Customizing nodes and links of the graph.
- Detecting user clicks on nodes to trigger specific actions.


However, I set myself a few constraints:

- Integration of the solution into my existing Angular 16 project.
- Avoiding direct manipulation of SVG, a skill I do not fully master.
- Ensuring the solution's robustness to handle all workflow possibilities, created by anyone.

My research led me to [npmjs.com](https://www.npmjs.com/), where I discovered the [@swimlane/ngx-graph library](https://www.npmjs.com/package/@swimlane/ngx-graph). Exploring its detailed documentation, I quickly realized that this solution met my needs.

## Integration of the Solution

After deciding to use @swimlane/ngx-graph, I proceeded to integrate it into my project. Here are the steps I followed:

- Installing the library using the npm i command.
- Importing the module into my project, managing lazy loading to optimize performance.
- Adding the <ngx-graph> tag to the HTML file of the component, defining nodes and links according to the provided examples.

And then, everything seemed to work perfectly at first glance!

**graph.ts:**

```typescript
import { Edge, Node } from '@swimlane/ngx-graph';

export const LINKS: Edge[] = [
  {
    id: 'link1',
    source: 'stepA',
    target: 'stepB',
  }
];

export const NODES: Node[] = [
  {
    id: 'stepA',
    label: 'Step A',
  },
  {
    id: 'stepB',
    label: 'Step B',
  }
];
```

**ngx-graph-component.html:**

```html
<ngx-graph
  [view]="view"
  [links]="links"
  [nodes]="nodes"
  (select)="onNodeSelect($event)"
>
  <ng-template #defsTemplate>
    <!-- def template (used by another template) -->
  </ng-template>

  <ng-template #nodeTemplate let-node>
    <!-- node template -->
  </ng-template>

  <ng-template #linkTemplate let-link>
    <!-- link template -->
  </ng-template>
</ngx-graph>
```


**ngx-graph-component.ts:**

```typescript
import { Component } from '@angular/core';
import { LINKS, NODES } from './graph';

@Component({
  selector: 'app-ngx-graph',
  templateUrl: './ngx-graph.component.html',
  styleUrls: ['./ngx-graph.component.scss'],
})
export class NgxGraphComponent {
  links = LINKS;
  nodes = NODES;
  view: [number, number] = [window.innerWidth, window.innerHeight];

  onNodeSelect($event: unknown) {
    // Do something when the node is selected
  }
}
```


## Encountered Challenges and Found Solutions

However, when attempting to customize nodes and links, I encountered my first obstacle: the arrowed bridges I added only partially appeared.

{% img image-width ngx-graph/error-only-1-edge.png All the links don't display %}

{% img image-width ngx-graph/error-transition.png Console error %}


After a quick investigation, I identified a conflict with other libraries using d3. Fortunately, I was able to resolve this issue 

* by downgrading the version of the library to make it compatible with Angular 16
* and in adding an override on the package.json file

```typescript
"overrides": {
    "d3-selection": "3.0.0"
}
```


Once this difficulty was overcome, I explored ways to customize nodes without directly manipulating SVG. I found an elegant solution by calling Angular components within SVG, allowing me to achieve graphics consistency with the rest of my application.


**ngx-graph-component.html:**

```html
<ng-template #nodeTemplate let-node>
    <svg>
      <foreignObject x="0" y="0" width="180" height="85">
        <ng-container *ngIf="node">
          <app-node-template [node]="node"></app-node-template>
        </ng-container>
      </foreignObject>
    </svg>
  </ng-template>
```

**Result of the implementation of the expected graph:**

{% img center image-width ngx-graph/final-graph.png graph %}

## Conclusion

In summary, using @swimlane/ngx-graph proved to be the perfect solution for my project. This library enabled me to create a dynamic, flexible, and interactive graph that met all my needs while seamlessly integrating with my Angular application.
If you are also faced with the challenge of implementing a dynamic graph in your Angular application, I highly recommend to try with @swimlane/ngx-graph!

[Acces to my gitlab project to have a complete example](https://gitlab.com/c.sureau/angular-ngx-graph)
