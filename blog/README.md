How to launch the project

```bash
npm install
```

```bash
hexo server
```

and then access with the browser to http://localhost:4000/


---

Quick Start

Create a new post

```bash
hexo new "My New Post"
```

Generate static files
1
$ hexo generate
